mafct:	sub  1,%sp		// place pour fd
	push 438		// 438 = 0666
	push 577		// 577 =  01101 = O\_WRONLY|O\_CREAT|O\_TRUNC
	push 2200		// adresse de la chaîne "toto" en mémoire
	ld   5,%a		// 5 = numéro de la primitive "open"
	trap			// \textbf{exception} \implique appel au noyau
	add  3,%sp		// dépiler les 3 arguments
	st   %a,(%sp)		// fd $\leftarrow$ retour de open
	ld   (%sp),%a		// valeur de retour $\leftarrow$ fd (optimisable)
	add  1,%sp		// libérer l'emplacement de fd
	rtn


