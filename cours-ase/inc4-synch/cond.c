#include <stdio.h>
#include <pthread.h>

pthread_cond_t  c ;		// condition POSIX
pthread_mutex_t m ;		// mutex associé à la condition
int var = 0 ;			// un exemple : on attend que var soit != 0

void attendre (void) {
  pthread_mutex_lock (&m) ;	// début de section critique
  while (var == 0)		// while : réveil possible pour d'autres raisons
    pthread_cond_wait (&c, &m) ;// libère la section critique pendant l'attente
  var = 0 ;			// l'événement est arrivé : on en tient compte
  pthread_mutex_unlock (&m) ;	// fin de section critique
}

void reveiller (void) {
  pthread_mutex_lock (&m) ;	// début de section critique
  var = 1 ;			// ... pas vraiment nécessaire ici
  pthread_cond_signal (&c) ;	// réveille un thread si besoin
  pthread_mutex_unlock (&m) ;	// fin de section critique
}

void *poireauteur (void *arg) {
    printf ("j'attends\n") ;
    attendre () ;
    printf ("je suis reveille\n") ;
}

void *reveilleur (void *arg) {
    printf ("je reveille\n") ;
    reveiller () ;
}

main () {
    pthread_t tp, tr ;
    pthread_mutex_init (&m, NULL) ;
    pthread_cond_init (&c, NULL) ;
    pthread_create (&tp, NULL, poireauteur, NULL) ;
    sleep (5) ;
    pthread_create (&tr, NULL, reveilleur, NULL) ;
    sleep (5) ;
}
