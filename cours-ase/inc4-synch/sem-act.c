P (S.compteur) {
    while (S.compteur <= 0)
	;        // attente active
    S.compteur-- ;
}

V (S.compteur) {
    S.compteur++ ;
}
