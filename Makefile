SUBDIRS = \
	conseils \
	cours-ase \
	cours-cse \
	cours-diu-eil \
	cours-ps \
	cours-sc \
	cours-se \
	exercices \
	manuel \
	mem-stage \
	z33refcard \

.PHONY: all $(SUBDIRS)

all: $(SUBDIRS)

conseils:
	cd conseils ; make

cours-ase:
	cd cours-ase ; make tout.pdf print-tout.pdf

cours-cse:
	cd cours-cse ; make tout.pdf print-tout.pdf

cours-diu-eil:
	cd cours-diu-eil ; make tout.pdf print-tout.pdf

cours-ps:
	cd cours-ps ; make tout.pdf print-tout.pdf

cours-sc:
	cd cours-sc ; make tout.pdf print-tout.pdf

cours-se:
	cd cours-se ; make tout.pdf print-tout.pdf

exercices:
	cd exercices ; make

manuel:
	cd manuel ; make

mem-stage:
	cd mem-stage ; make

z33refcard:
	cd z33refcard ; make

index.html: README.md
	pandoc \
	    --standalone \
	    --metadata="pagetitle:Supports d'enseignement de Pierre David" \
	    --variable='lang:fr' \
	    --variable='author-meta:Pierre David' \
	    --variable='keywords:operating system' \
	    --variable='keywords:unix' \
	    --variable='highlighting-css: *{font-family:Arial,sans-serif;}' \
	    -o index.html \
	    README.md

clean:
	for i in $(SUBDIRS) ; do (cd $$i ; make clean) ; done
	rm -f index.html
