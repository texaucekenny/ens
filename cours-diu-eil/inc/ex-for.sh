#!/bin/sh

N=1
for arg
do
    if [ -f "$arg" ]
    then
       echo "$N: $arg est un fichier"
    elif [ -d "$arg" ]
    then
       echo "$N: $arg est un repertoire"
    else
       echo "$N: $arg est autre chose"
    fi
    N=$((N+1))
done

exit 0
